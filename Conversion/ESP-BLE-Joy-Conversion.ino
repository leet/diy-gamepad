const unsigned long POLLING_INTERVAL = 1000U / 50U;

// LED_BUILTIN
#define LED_PIN 22
int ledState = LOW;

// Send debug messages to serial port
#define ENABLE_SERIAL_DEBUG

#ifdef ENABLE_SERIAL_DEBUG
  #define dstart(spd) do {Serial.begin (spd); while (!Serial) {digitalWrite (LED_PIN, (millis () / 500) % 2);}} while (0);
  #define debug(...) Serial.print (__VA_ARGS__)
  #define debugln(...) Serial.println (__VA_ARGS__)
#else
  #define dstart(...)
  #define debug(...)
  #define debugln(...)
#endif

#include <Bounce2.h>      // https://github.com/thomasfredericks/Bounce2
#include <BleGamepad.h>   // https://github.com/lemmingDev/ESP32-BLE-Gamepad

// Instantiate Bounce array
#define NUM_BUTTONS 4
const uint8_t BUTTON_PINS[NUM_BUTTONS] = {25, 26, 32, 33};  // https://randomnerdtutorials.com/esp32-pinout-reference-gpios/
const uint8_t BUTTON_BLE[NUM_BUTTONS] = {DPAD_UP, DPAD_DOWN, DPAD_LEFT, DPAD_RIGHT};  // https://randomnerdtutorials.com/esp32-pinout-reference-gpios/
const char *BUTTON_NAME[NUM_BUTTONS] = {"DPAD_UP", "DPAD_DOWN", "DPAD_LEFT", "DPAD_RIGHT"};  // https://randomnerdtutorials.com/esp32-pinout-reference-gpios/

const int delayBetweenHIDReports = 50;    // Additional delay in milliseconds between HID reports
const int delayBounce = 5;               // Bounce delay

// Battery Emulation stuff
char BLEBattery = 80;
char BLEBattDirect = 1;
unsigned long previousMillis = 0;        // will store last time LED was updated

// the follow variables is a long because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long interval = 10000;                    // interval at which to blink (milliseconds)

Bounce * buttons = new Bounce[NUM_BUTTONS];

BleGamepad bleGamepad("LeeNX-Joystick", "LeeNX-Manufacturer", BLEBattery);


void setup() {
  // Lit the builtin led whenever buttons are pressed
  pinMode (LED_PIN, OUTPUT);

  dstart (115200);

  debugln (F("Starting ..."));

  debugln (F("Config joystick"));

  for (int i = 0; i < NUM_BUTTONS; i++) {
    buttons[i].attach( BUTTON_PINS[i] , INPUT_PULLUP );     //setup the bounce instance for the current button
    //buttons[i].attach( BUTTON_PINS[i] , INPUT );            //setup the bounce instance for the current button
    buttons[i].interval(delayBounce);                       // interval in ms
    debug (F("\t Name: \t")); debug (BUTTON_NAME[i]);
    debug (F("\t Num: \t")); debug (i);
    debug (F("\t Pin: \t")); debug (BUTTON_PINS[i]);
    debug (F("\t BLE: \t")); debug (BUTTON_BLE[i]);
    debugln (F(" "));
  }

  delay(3000); // 3 second delay for recovery

  digitalWrite(LED_PIN, ledState);

  debugln (F("BLE starting"));
  bleGamepad.begin();

  debugln (F("Ready!"));

}

void loop() {

/*
  static unsigned long last = 0;
  
  if (millis () - last >= POLLING_INTERVAL) {
    last = millis ();
    debugln (F("Pole"));
  } 
*/
//  else {
    /* Flash led with buttons, I like this but it introduces a bit of
     * lag, so let's keep it disabled by default
     */
    // Toggle LED state :
/*
    ledState = !ledState;
    digitalWrite (LED_PIN, ledState);    
  }
*/

  if(bleGamepad.isConnected()) {
    //bleGamepad.setAxes(0, 0, 0, 0, 0, 0, DPAD_CENTERED);

    bool needToToggleLed = false;
    
    for (int i = 0; i < NUM_BUTTONS; i++)  {
      // Update the Bounce instance :
      boolean changed = buttons[i].update();
      if ( changed ) {
        int value = buttons[i].read();  // Get the updated value
        //boolean value = buttons[i].read();  // Get the updated value

        debugln (F(" changed "));
        debug (F("\t Name: \t")); debug (BUTTON_NAME[i]);
        debug (F("\t Num: \t")); debug (i);
        debug (F("\t Pin: \t")); debug (BUTTON_PINS[i]);
        debug (F("\t BLE: \t")); debug (BUTTON_BLE[i]);
        debug (F("\t State: \t")); debug (value);

        needToToggleLed = true;

        if ( value == HIGH ) {
          //bleGamepad.press(BUTTON_BLE[i]);
          bleGamepad.setAxes(0, 0, 0, 0, 0, 0, BUTTON_BLE[i]);
          //bleGamepad.setHat(BUTTON_BLE[i]);
        } else {
          //bleGamepad.release(BUTTON_BLE[i]);
          bleGamepad.setAxes(0, 0, 0, 0, 0, 0, DPAD_CENTERED);
        }
      }
    }

    // if a LED toggle has been flagged :
    if ( needToToggleLed ) {
      // Toggle LED state :
      ledState = !ledState;
      digitalWrite(LED_PIN, ledState);
      needToToggleLed = false;
    }

    // Emulate changing battery
    unsigned long currentMillis = millis();
    
    if ( (unsigned long)(currentMillis - previousMillis) >= interval ) {
      previousMillis = currentMillis;    
      bleGamepad.setBatteryLevel(BLEBattery);
      BLEBattery += BLEBattDirect;
      if ( BLEBattery > 95 ) {
        BLEBattDirect = -1;
      }
      if ( BLEBattery < 15 ) {
        BLEBattDirect = 1;
      }
    }

//    debug (F("\t battery: \t")); debug(BLEBattery, DEC);
//    debug (F("\t Cmills: \t"));  debug(currentMillis, DEC);
//    debug (F("\t Pmills: \t"));  debug(previousMillis, DEC);
    
    debugln (F(" "));
    
    // Need to Fix? - Can't use delays. but millis
    delay(delayBetweenHIDReports);
  } else {
    debugln (F("BLE not connected"));
    delay(500);
  }
}
