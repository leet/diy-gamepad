/*
 * A simple sketch that maps 2 analog vaules reported by ADS1x15 
 * via I2C on ESP32 to 2 axis of joystick on the controller and
 * digital button
 */

#include <Wire.h>
#include <Adafruit_ADS1015.h>

Adafruit_ADS1115 ads;  /* Use this for the 16-bit version */
//Adafruit_ADS1015 ads;     /* Use thi for the 12-bit version */
const float multiplier = 0.1875F; /* GAIN_TWOTHIRDS */

// Detect the falling edge of multiple buttons.
// Eight buttons with internal pullups.
// Toggles a LED when any button is pressed.

// Include the Bounce2 library found here :
// https://github.com/thomasfredericks/Bounce2
#include <Bounce2.h>      // https://github.com/thomasfredericks/Bounce2
#include <BleGamepad.h>   // https://github.com/lemmingDev/ESP32-BLE-Gamepad

#define LED_PIN 13
int ledState = LOW;

// Instantiate a Bounce object
#define NUM_BUTTONS 8
const uint8_t BUTTON_PINS[NUM_BUTTONS] = {16, 17, 18, 19, 20, 21, 22, 23};  // https://randomnerdtutorials.com/esp32-pinout-reference-gpios/
const uint8_t BUTTON_BLE[NUM_BUTTONS] = {BUTTON_1, BUTTON_2, BUTTON_3, BUTTON_4, BUTTON_5, BUTTON_6, BUTTON_7, BUTTON_8};  // https://randomnerdtutorials.com/esp32-pinout-reference-gpios/

const int delayBetweenHIDReports = 5; // Additional delay in milliseconds between HID reports

// Battery Emulation stuff
char BLEBattery = 80;
char BLEBattDirect = 1;
long previousMillis = 0;        // will store last time LED was updated
 
// the follow variables is a long because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long interval = 1000;           // interval at which to blink (milliseconds)

 
Bounce * buttons = new Bounce[NUM_BUTTONS];

BleGamepad bleGamepad("LeeNX Device Name", "LeeNX Device Manufacturer", BLEBattery);


void setup() 
{

  Serial.begin(115200);
  Serial.println("Starting LeeNX BLE starting ...");

  for (int i = 0; i < NUM_BUTTONS; i++) {
    //buttons[i].attach( BUTTON_PINS[i] , INPUT_PULLUP  );  //setup the bounce instance for the current button
    buttons[i].attach( BUTTON_PINS[i] , INPUT  );           //setup the bounce instance for the current button
    buttons[i].interval(5);                                 // interval in ms
  }

  // Setup the LED :
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, ledState);

  bleGamepad.begin();

  Serial.println("Starting LeeNX BLE working ...");

  // The ADC input range (or gain) can be changed via the following
  // functions, but be careful never to exceed VDD +0.3V max, or to
  // exceed the upper and lower limits if you adjust the input range!
  // Setting these values incorrectly may destroy your ADC!
  //                                                                ADS1015  ADS1115
  //                                                                -------  -------
  // ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
  // ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV
  // ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
  // ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
  // ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
  // ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV

  ads.setGain(GAIN_TWOTHIRDS); // works
  //ads.setGain(GAIN_ONE);  // volatge setup?
  ads.begin();
  Serial.println("Getting single-ended readings from AIN0..3");
  
}

void loop() 
{
  if(bleGamepad.isConnected()) {

    int16_t adc0, adc1, adc2, adc3;
    adc0 = ads.readADC_SingleEnded(0);
    adc1 = ads.readADC_SingleEnded(1);
    adc2 = ads.readADC_SingleEnded(2);
    adc3 = ads.readADC_SingleEnded(3);

    Serial.print("\t rAIN0: \t"); Serial.print(adc0, DEC);
    Serial.print("\t rAIN1: \t"); Serial.print(adc1, DEC);
    //Serial.print("\t rAIN2: \t"); Serial.print(adc2, HEX);
    //Serial.print("\t rAIN3: \t"); Serial.println(adc3, HEX);

    int16_t madc0, madc1, madc2, madc3; 

    // ToDo: Look at shifting bits?
    // Map analog reading from 0 ~ 4095 to 32737 ~ -32737 for use as an axis reading
    //madc0 = map(adc0, 0, 17000, 32737, -32737);
    //madc1 = map(adc1, 0, 17000, 32737, -32737);
    madc0 = map(adc0, 0, 18000, -32737, 32737);
    madc1 = map(adc1, 0, 18000, -32737, 32737);
    madc2 = map(adc2, 0, 18000, -32737, 32737);
    madc3 = map(adc3, 0, 18000, -32737, 32737);

    Serial.print("\t mAIN0: \t"); Serial.print(madc0, DEC);
    Serial.print("\t mAIN1: \t"); Serial.print(madc1, DEC);
    //Serial.print("\t mAIN2: \t"); Serial.print(madc2, HEX);
    //Serial.print("\t mAIN3: \t"); Serial.println(madc3, HEX);

    bleGamepad.setAxes(madc0, madc1, madc2, madc3, 0, 0, DPAD_CENTERED);

    bool needToToggleLed = false;
    
    for (int i = 0; i < NUM_BUTTONS; i++)  {
      // Update the Bounce instance :
      boolean changed = buttons[i].update();
      if ( changed ) {
        int value = buttons[i].read();  // Get the updated value

        Serial.println(" changed ");
        needToToggleLed = true;

        if ( value == HIGH ) {
          bleGamepad.press(BUTTON_BLE[i]);
        }
        else
        {
          bleGamepad.release(BUTTON_BLE[i]);
        }
      }
    }

    // if a LED toggle has been flagged :
    if ( needToToggleLed ) {
      // Toggle LED state :
      ledState = !ledState;
      digitalWrite(LED_PIN, ledState);
    }

    // Emulate changing battery
    unsigned long currentMillis = millis();
    
    if ( currentMillis - previousMillis > interval ) {
      previousMillis = currentMillis;    
      bleGamepad.setBatteryLevel(BLEBattery);
      BLEBattery += BLEBattDirect;
      if ( BLEBattery > 95 ) {
        BLEBattDirect = -1;
      }
      if ( BLEBattery < 15 ) {
        BLEBattDirect = 1;
      }
    }

    Serial.print("\t battery: \t"); Serial.print(BLEBattery, DEC);
    Serial.print("\t Cmills: \t"); Serial.print(currentMillis, DEC);
    Serial.print("\t Pmills: \t"); Serial.print(previousMillis, DEC);
    
    Serial.println(" ");


    // Need to Fix? - Can't use delays. but millis
    delay(delayBetweenHIDReports);
  }
  else
  {
    Serial.println("BLE not connected");
    delay(500);
  }
}
