Need to spend some time on something new and fun, have some parts. Come across
a nice ESP32 Lib for BLE support, reporting Gamepad/joystick.

I have an old PS2 wired game controller, would be nice for MAME type games and
without cables, the computer and displays should be safer.

With very little knowledge, but interesting in building something "cool" or
that might be used, here we go.

# DIY Gamepad
Project is based off the ESP32 BLE Gamepad library - https://github.com/lemmingDev/ESP32-BLE-Gamepad

## List of Testing tools
Web Testing tools:
 * https://gamepad-tester.com/
 * https://github.com/luser/gamepadtest for http://luser.github.io/gamepadtest/
 * https://gamepadviewer.com/
 * https://apps.nektro.net/gamepad/
 * https://gamepad-demo.glitch.me/ has vibtartion test button for my test PS4 controller - https://glitch.com/edit/#!/gamepad-demo

Command line testing:
https://github.com/Grumbel/sdl-jstest
```
leet-mbp:build leet$ ./sdl2-jstest -l
Found 1 joystick(s)

Joystick Name:     'Manufacturer LeeNX Device Name'
Joystick GUID:     0300000002e50000abcd000001100000
Joystick Number:    0
Number of Axes:     6
Number of Buttons: 32
Number of Hats:     2
Number of Balls:    0
GameControllerConfig:
  missing (see 'gamecontrollerdb.txt' or SDL_GAMECONTROLLERCONFIG)
```

I would like to add a python joystick/gamepad testing tool, which should work on
all platforms - OSX/Linux/Windows possible Android?

# Testing Code
ESP32-i2c-scanner: make sure we can see an i2c device, at the address of ADS1x15
ESP32-i2c-ads1x15: try and read some vaules from ADS1x15

# ToDo:
 * Add circuit diagram
 * Add images of setup
 * Add secure pairing?
 * ESP32 stats?

## Nice to haves
 Rechargerable batter:
  * with reporting levels to connected hosts
  * SparkFun Battery Babysitter
  * local battery level feedback - LED level indicator
  * LiPo Battery - https://learn.sparkfun.com/tutorials/battery-babysitter-hookup-guide

BLE Pairing Process or reset
Limit device report back: ie only the buttons and axixs used
BLE limit HID reporting: possible tie analog/digital input to max HID report rate
Debounce digital input and oversample analogs for neater inputs
Programablity: ie changable rates improvements for battery
Live debugging + Config improvements?
Reduced power consumption? Battery Bank turning off?
LED User name Feedback (4xARGB)
ForceFeedBack - Recovered mobile device rumbler or haptic feedback device

# Crazy
 Add IMU like Adafruit 10-DOF IMU or similar and add extra inputs control support
 Audio IN and OUT? ;-0

 I shoud cost all the bits, just so that I can compare to what I should have
 bought, instead of learn lots and play as much.

 Screen for user feedback
